<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('get-pratix-api', function()
{  
    $client = new \GuzzleHttp\Client();
    $url = "https://api.pratix.top/api/geo/get/simple/radio/40/-3.7553375/-38.6296543";

    $response = $client->request("GET", $url);


	$responseArray = json_decode($response->getBody(), true);
	$responseArray = $responseArray['data'];
	//dd($responseObject);exit;
    foreach($responseArray as $key => $value) {
		$responseArray[$key]['payments']['name'] = 'Profissional ' . $value['payments']['name'];
		$responseArray[$key]['atuacao']['total_de_servicos'] = count($value['atuacao']['servicos']);
	}
	
	echo json_encode($responseArray);
	exit;
});